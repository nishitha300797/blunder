// By importing this creates an express application
import express from "express";

// It is responsible for parsing the incoming request bodies in a middleware before handling it.
import bodyParser from "body-parser";

// CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.
import cors from "cors";

import {
  register_user_handler,
  login_user_handler,
  forgot_user_handler,
  cards_hadler,
  get_cards_handler
} from "./path_handlers/user_path.js";
// import{ get_cards} from "./path_handlers/cards_path"
import { _validate_password } from "./utilities/authentication_validation.js";

// Creating an instance of express
const app = express();

// Returns middleware that only parses jsonencoded bodies
app.use(bodyParser.json());

// Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

// It is a mechanism to allow or restrict requested resources on a web server depend on where the HTTP request was initiated.
app.use(
  cors({
    origin: "*",
  })
);

/**
 * posting the register data using post method
 * @path /register is the path by using this path we can post the user register data
 * @register_user_handler by calling this function we can validate the user register data
 */
app.post("/register", register_user_handler);

/**
 * posting the login data using post method
 * @path /login is the path by using this path we can post the user login data
 * @login_user_handler by calling this function we can validate the user login data
 */
app.post("/login", login_user_handler);

/**
 * resetting the forgot password data using post method
 * @path /forget is the path by using this path we can reset the user password
 * @forgot_user_handler by calling this function we can validate the forgot password data
 */
app.post("/forget", forgot_user_handler);

app.post("/card",cards_hadler);

app.get("/get_cards",get_cards_handler)
// the server listening the port 3000
app.listen(8000, () => {
  console.log(`listening at port 8000`);
});

