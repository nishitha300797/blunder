// importing mongoose from mongoose
import mongoose from "mongoose";

// importing the internal modules
// import { UserRegisterSchema, UserLoginSchema } from "./Schema.js";

// conneting to database
mongoose.connect(
  "mongodb+srv://arunchand777:Arunchand@cluster0.ucw6a.mongodb.net/blunder?retryWrites=true&w=majority"
);
import {
  UserRegisterSchema,UserLoginSchema,CardSchema
} from './Schema.js'

//conneting to database
// mongoose.connect("mongodb+srv://nmanchiravula:Nishitha1997@cluster0.j8eac.mongodb.net/Blunder?retryWrites=true&w=majority")


// mongoose.connect(
//   "mongodb+srv://arunchand777:Arunchand@cluster0.ucw6a.mongodb.net/blunder?retryWrites=true&w=majority"
// );

/* creating a model for the user_register_data for creating a list and enter objects in it */
const UserRegisterData = mongoose.model("UserRegisterData", UserRegisterSchema);

/* creating a model for the user_login_data for storing the user login details */
const UserLoginData = mongoose.model("UserLoginData", UserLoginSchema);

/*creating a model for the cards  */
const CardData = mongoose.model("CardData",CardSchema);

function inserting_register_data_into_database(user_data) {
  const UserData = new UserRegisterData({
    name: user_data.name,
    username: user_data.username,
    password: user_data.password,
    re_enter_password: user_data.re_enter_password,
    email: user_data.email,
    age: user_data.age,
    phone_no: user_data.phone_no,
    security_question: user_data.security_question,
    security_answer: user_data.security_answer,
    image: user_data.image,
  });
  // saving the data
  UserData.save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

function inserting_cards(cards_data){
  const CardsData = new CardData({
    name:cards_data.name,
    imgUrl:cards_data.imgUrl
  });
  CardsData.save().then(()=>console.log("cards added")).catch((err)=>console.log(err))
}

/**
 * Inserting the user login data into the database
 * @param login_data database contains the user logged in data
 */
function inserting_user_login_data_into_database(login_data) {
  const UserLogin = new UserLoginData({
    username: login_data.username,
    password: login_data.password,
    // jwt_token: login_data.jwt_token,
  });
  // saving the data
  UserLogin.save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}


export {UserLoginData,UserRegisterData,inserting_register_data_into_database,inserting_user_login_data_into_database,inserting_cards,CardData}
