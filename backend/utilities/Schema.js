// importing mongoose from mongoose
import mongoose from "mongoose";

/*
  creating a schema for user_register_data
*/
const UserRegisterSchema = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  re_enter_password: String,
  email: String,
  age: Number,
  phone_no: Number,
  security_question: String,
  security_answer: String,
  image: String,
});

/* 
    creating a schema for the user_login_data
*/
const UserLoginSchema = new mongoose.Schema({
  username: String,
  password: String,
  jwt_token: String,
});

/*
    creating a schema for MATCH_REQUEST
    with respect to datatypes
*/
const MATCH_REQUEST = new mongoose.Schema({
  match_request_from: String,
  match_request_to: String,
  match_request_sender: String,
  match_request_receiver: String,
  match_request_status: Number,
  created_date: Date,
  accepted_date: Date,
});

/*
    creating a schema for RECOMMEND_USERS
    with respect to datatypes
*/
const RECOMMEND_USERS = new mongoose.Schema({
  age: Number,
  fullname: String,
  gender: String,
});

/*
    creating a schema for CHATS
    with respect to datatypes
*/
const CHATS = new mongoose.Schema({
  username: String,
  image: String,
  status: String,
  lastSeen: String,
});

/*
    creating a schema for EDITPROFILE
    with respect to datatypes
*/
const EDITPROFILE = new mongoose.Schema({
  image: String,
  fullname: String,
  username: String,
  email: String,
  password: String,
  interestedin: String,
  passion: Array,
  gender: String,
});

/* creating schema for Cards*/
const CardSchema = mongoose.Schema({
  name: String,
  imgUrl: String,
});
export { UserRegisterSchema, UserLoginSchema, CardSchema };
