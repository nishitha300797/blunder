/**
 * It is used to get the cards using get method
 * @param {1} request this is used to get the cards
 * @param {2} response which is the response object that send the response to the client
 */

const get_cards = (request, response) => {
  CARDS.find({}, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      response.send(data);
    }
  });
};

export { get_cards };
