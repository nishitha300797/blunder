import { response } from "express";
import {
  validate_register_data,
  _validate_password,
  validate_user_register_database,
  validate_user_login_data,
  validate_user_login_data_for_forget_password,
} from "../utilities/authentication_validation.js";
import {
  inserting_cards
} from "../utilities/mongoDb.js"
import { CardData } from "../utilities/mongoDb.js";
/**
 * @path /register is the path by using this path we can post the user register data
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
const register_user_handler = (request, response) => {
  // register_data is the variable that contains the request body data
  const register_data = request.body;
  // validate_register_data function returns an object that contains the status_code and the status message
  let validation_result = validate_register_data(register_data);
  // if returned result doesn't contains 200 status code the post method returns the error response
  if (validation_result.status_code !== 200) {
    let { status_message } = validation_result;
    // sending the response to the client
    response.send(JSON.stringify(validation_result));
  }
  // if validate_register_data does contains 200 status code it goes to this block
  else {
    // calling the validate_user_register_database that are defined in the authentication_validation.js
    validate_user_register_database(register_data, response, validation_result);
  }
};

/**
 * @path /login is the path by using this path we can post the user login data and validate it using the register data
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
const login_user_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const login_data = request.body;
  // initializing the count is 0 because we need to validate the request parameters
  var count = 0;
  // iteraing over the body data because we need to count the body params
  for (let item in login_data) {
    // updating the count
    count = count + 1;
  }
  //we need only username and password for user login
  //if any extra params are passed in the request it throws error
  if (count !== 2) {
    response.send(
      JSON.stringify({
        status_code: 400,
        status_message: "Please enter valid parameter count",
      })
    );
  }
  // if parameter count is valid execution goes through the else block
  else {
    validate_user_login_data(login_data, response);
  }
};

/**
 * @path /forget/user_type is the path by using this path we can validate the user data and update the user password
 * @param request is the callback parameter that contains the data that passed from client
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
const forgot_user_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const login_data = request.body;
  // client_key is the jwt token only logedin user are access the data
  const { client_key } = request.headers;
  // initializing the count is 0 because we need to validate the request parameters
  var count = 0;
  // iterating over the body data because we need to count the body params
  for (let item in login_data) {
    // updating the count
    count = count + 1;
  }
  //we need only username and password for user login
  //if any extra params are passed in the request it throws error
  if (count !== 2) {
    response.send(JSON.stringify("Please enter valid parameter count"));
  }
  // if parameter count are valid execution goes through the else block
  else {
    validate_user_login_data_for_forget_password(
      login_data,
      response,
      client_key
    );
  }
};

const cards_hadler = (request,response)=>{
  const cards_data = request.body;
  let count = 0
  for(let item in cards_data){
    count = count + 1
  }
  if(count !== 2){
    response.send(JSON.stringify("Please enter valid parameter count"))
  }
  else{
      inserting_cards(cards_data)
      response.send(JSON.stringify("Card added to database"))
  }
}

const get_cards_handler = (request,response)=>{
  CardData.find((err, data) => {
    // if there is error
    if (err) {
      // set response to 500, which means internal server error and send error back
      response.status(500).send(err);
    } else {
      // 200 means found
      response.status(200).send(data);
    }
  });
}

export { register_user_handler, login_user_handler, forgot_user_handler,cards_hadler,get_cards_handler };
