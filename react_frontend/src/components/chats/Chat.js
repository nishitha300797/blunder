// External Dependencies
import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { Link } from "react-router-dom";

// Internal Dependecies
import "./Chat.css";

// Declaring a function called Chat
function Chat({ name, message, profilePic, timestamp }) {
  return (
    //Link provides declarative, accessible navigation around your application.
    <Link to={`/chats/${name}`}>
      <div className="chat">
        <Avatar className="chat__image" src={profilePic} />
        <div className="chat__details">
          <h2>{name}</h2>
          <p>{message}</p>
        </div>
        <p className="chat__timestamp">{timestamp}</p>
      </div>
    </Link>
  );
}

// Exporting the above Chat function.
export default Chat;
