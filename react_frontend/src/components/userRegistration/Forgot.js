// External Dependencies
import { React, Component } from "react";
import Userfront from "@userfront/core";
// Internal Dependecies
import "./forgot.css";
import { BottomNavigation } from "@material-ui/core";

/* Userfront gives you secure,pre-configured authentication 
  & authorization that stays up to date automatically. 
  */
Userfront.init("demo1234");
// Declaring Forgot class
class Forgot extends Component {
  // creating a constructor
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      passwordVerify: "",
      alertMessage: "",
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setAlertMessage = this.setAlertMessage.bind(this);
  }

  // Whenever an input changes value, change the corresponding state variable
  handleInputChange(event) {
    event.preventDefault();
    const target = event.target;
    this.setState({
      [target.name]: target.value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    // Reset the alert to empty
    this.setAlertMessage();

    // Verify that the passwords match
    if (this.state.password !== this.state.passwordVerify) {
      return this.setAlertMessage("Passwords must match");
    }

    // Call Userfront.resetPassword()
    Userfront.resetPassword({
      password: this.state.password,
    }).catch((error) => {
      this.setAlertMessage(error.message);
    });
  }

  setAlertMessage(message) {
    this.setState({ alertMessage: message });
  }

  render() {
    return (
      <div id="forgot-container">
        <Alert message={this.state.alertMessage} />
        <h1 style={{textAlign:"center",margin:"10vh"}}>Forgot Password </h1>
        <form onSubmit={this.handleSubmit}>
          <label>
            Security Question
            <input
              name="passwordVerify"
              type="text"
              // value={this.state.passwordVerify}
              // onChange={this.handleInputChange}
            />
          </label>
          <label>
            Password
            <input
              name="password"
              type="password"
              value={this.state.password}
              onChange={this.handleInputChange}
            />
          </label>
          <label>
            Re-type password
            <input
              name="passwordVerify"
              type="password"
              value={this.state.passwordVerify}
              onChange={this.handleInputChange}
            />
          </label>
          <button type="submit">Reset password</button>
        </form>
      </div>
    );
  }
}

// Define the alert component
class Alert extends Component {
  constructor(props) {
    super(props);
  }

  // Render the password reset form
  render() {
    if (!this.props.message) return "";
    return <div id="alert">{this.props.message}</div>;
  }
}

// Exporting the above Forgot class.
export default Forgot;
