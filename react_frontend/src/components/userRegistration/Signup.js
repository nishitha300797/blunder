// External Dependencies
import { Component } from "react";
// Internal Dependecies
import "./Signup.css";

// Declaring SignUp class
class SignUp extends Component {
  state = {
    name: "",
    username: "",
    password: "",
    email: "",
    age: "",
    phone_no: "",
    re_enter_password: "",
    security_question: "",
    security_answer: "",
    image: "",
    error_msg: "",
    is_signup: false,
  };

  onChangeName = (event) => {
    this.setState({ name: event.target.value });
    console.log(event.target.value);
  };
  onChangeUserName = (event) => {
    this.setState({ username: event.target.value });
    console.log(event.target.value);
  };

  onChangePassword = (event) => {
    this.setState({ password: event.target.value });
    console.log(event.target.value);
  };

  onChangeReEnterPassword = (event) => {
    this.setState({ re_enter_password: event.target.value });
    console.log(event.target.value);
  };

  onChangeEmail = (event) => {
    this.setState({ email: event.target.value });
    console.log(event.target.value);
  };

  onChangeAge = (event) => {
    this.setState({ age: event.target.value });
    console.log(event.target.value);
  };

  onChangePhoneNo = (event) => {
    this.setState({ phone_no: event.target.value });
    console.log(event.target.value);
  };

  onChangeSecurityQuestion = (event) => {
    this.setState({ security_question: event.target.value });
    console.log(event.target.value);
  };
  onChangeSecurityAnswer = (event) => {
    this.setState({ security_answer: event.target.value });
    console.log(event.target.value);
  };

  onChangeImage = (event) => {
    this.setState({ image: event.target.value });
    console.log(event.target.value);
  };
  // After submitting the signup form redirects to login page.
  redirectToSignin = () => {
    const { history } = this.props;
    history.replace("/login");
  };
  //  Submit form for SignUp credentials
  submitingSignup = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8000/register";
    const {
      name,
      username,
      password,
      re_enter_password,
      email,
      age,
      phone_no,
      security_question,
      security_answer,
      image,
    } = this.state;
    const userDetails = {
      name,
      username,
      password,
      re_enter_password,
      age,
      email,
      phone_no,
      security_question,
      security_answer,
      image,
    };
    console.log(userDetails);
    const option = {
      method: "POST",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    console.log(response);
    const data = await response.json();
    console.log(data);
    if (data.status_code !== 200) {
      this.setState({ error_msg: data.status_message, is_signup: true });
    } else {
      this.redirectToSignin();
    }
  };

  render() {
    const {
      name,
      username,
      password,
      re_enter_password,
      email,
      age,
      phone_no,
      security_question,
      security_answer,
      image,
      error_msg,
      is_signup,
    } = this.state;
    return (
      <section className="login">
        <div className="container">
          <h3 className="title">Sign up</h3>
          <form onSubmit={this.submitingSignup}>
            <div className="form-group">
              <label htmlFor="fullname" />
              <input
                type="text"
                id="fullname"
                placeholder="Name"
                onChange={this.onChangeName}
                value={name}
              />
            </div>
            <div className="form-group">
              <label htmlFor="username" />
              <input
                type="text"
                id="username"
                placeholder="User Name"
                onChange={this.onChangeUserName}
                value={username}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password" />
              <input
                type="password"
                id="password"
                placeholder="Password"
                onChange={this.onChangePassword}
                value={password}
              />
            </div>
            <div className="form-group">
              <label htmlFor="reEnterPassword" />
              <input
                type="password"
                id="reEnterPassword"
                placeholder="re-enter-password"
                onChange={this.onChangeReEnterPassword}
                value={re_enter_password}
              />
            </div>
            <div className="form-group">
              <label htmlFor="Email" />
              <input
                type="Email"
                id="Email"
                placeholder="Email"
                onChange={this.onChangeEmail}
                value={email}
              />
            </div>
            <div className="form-group">
              <label htmlFor="age" />
              <input
                type="text"
                id="age"
                placeholder="Age"
                onChange={this.onChangeAge}
                value={age}
              />
            </div>
            <div className="form-group">
              <label htmlFor="phone no" />
              <input
                type="text"
                id="phone no"
                placeholder="Phone No"
                onChange={this.onChangePhoneNo}
                value={phone_no}
              />
            </div>
            <div className="form-group">
              <label htmlFor="security question" />
              <input
                type="text"
                id="security question"
                placeholder="Security Question"
                onChange={this.onChangeSecurityQuestion}
                value={security_question}
              />
            </div>
            <div className="form-group">
              <label htmlFor="security answer" />
              <input
                type="text"
                id="security answer"
                placeholder="Security Question"
                onChange={this.onChangeSecurityAnswer}
                value={security_answer}
              />
            </div>
            <div className="form-group" action="/action_page.php">
              <label for="img" />
              <input
                type="text"
                id="img"
                placeholder="paste your image url"
                onChange={this.onChangeImage}
                value={image}
              />
            </div>
            <button type="submit" className="button123">
              Sign up
            </button>
            {is_signup && <p className="register-error">* {error_msg}</p>}
          </form>
          <p className="additional-act">
            Already have an account?{" "}
            <span onClick={this.redirectToSignin}> Sign in </span>
          </p>
        </div>
      </section>
    );
  }
}

// Exporting the above SignUp class.
export default SignUp;
