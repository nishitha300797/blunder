// External Dependencies
import { Component, React } from "react";
// Internal Dependecies
import {Link} from 'react-router-dom'
import "./login.css";

// Declaring LogIn class
class LogIn extends Component {
  state = {
    username: "",
    password: "",
    error_msg: "",
    is_login: false,
  };
  // If the user doesn't have an acccount then redirects to signup page.
  redirectToSignup = () => {
    const { history } = this.props;
    history.replace("/signup");
  };
  // If the user enters valid credentials, then directs to home page.
  loginSuccess = (data) => {
    const { history } = this.props;
    history.replace("/home");
  };
  //  Submit form for Login credentials
  submitForm = async (event) => {
    console.log("hello");
    event.preventDefault();
    const { username, password } = this.state;
    const userDetails = { username, password };
    const url = "http://localhost:8000/login";
    const options = {
      method: "POST",
      body: JSON.stringify(userDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, options);
    const data = await response.json();
    console.log(data);
    if (data.status_code !== 200) {
      this.setState({ error_msg: data.status_message, is_login: true });
    } else {
      this.loginSuccess(data);
    }
  };

  onChangeUsername = (event) => {
    this.setState({ username: event.target.value });
    console.log(event.target.value);
  };

  onChangePassword = (event) => {
    this.setState({ password: event.target.value });
    console.log(event.target.value);
  };
  render() {
    const { username, password, is_login, error_msg } = this.state;
    return (
      <div className="login-container">
        <div className="form-box">
          <div className="header-form">
            <h2>
              Enter Login Credentials!
              {/* <i className="fa fa-user-circle user-icon"></i> */}
            </h2>
            <div className="image"></div>
          </div>
          <div className="body-form">
            <form onSubmit={this.submitForm}>
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Username"
                  value={username}
                  onChange={this.onChangeUsername}
                />
              </div>
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Password"
                  value={password}
                  onChange={this.onChangePassword}
                />
              </div>
              <button type="submit" className="">
                LOGIN
              </button>
              {is_login && <p className="register-error">*{error_msg}</p>}
              <div className="message">
               <Link to='/forgot'>
               <div>
                  <a href="#">Forgot your password</a>
                </div>
               </Link>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

// Exporting the above LogIn class.
export default LogIn;
