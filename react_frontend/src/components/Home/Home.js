import {Component}  from 'react'

import Header from '../header/Header'

import TinderCards from '../tinderCards/TinderCards'

import SwipeButtons from '../swipeButtons/SwipeButtons'

class Home extends Component{

    render(){

        return(

            <>

            <Header/>

            <TinderCards />

            <SwipeButtons/>

            </>

        )

    }

}



export default Home;