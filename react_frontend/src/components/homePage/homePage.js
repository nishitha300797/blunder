// External Dependencies
import {Component}  from 'react';
// Internal Dependecies
import Header from "../header/Header";
import TinderCards from "../tinderCards/TinderCards";
import SwipeButtons from "../swipeButtons/SwipeButtons";
// Declaring Home class
class Home extends Component {
  render() {
    return (
      <>
        <Header />
        <TinderCards />
        <SwipeButtons />
      </>
    );
  }
}

// Exporting the above Home class.
export default Home;
