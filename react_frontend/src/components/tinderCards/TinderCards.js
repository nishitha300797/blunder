// External Dependencies
import React, { useEffect, useState } from "react";
// Internal Dependencies
import axios from 'axios';
import TinderCard from 'react-tinder-card'
import "./TinderCards.css";

// Declaring TinderCards as a function which returns div element.
function TinderCards() {
    const [people, setPeople] = useState([])
    // useEffect(() => {
    //     database.collection('people').onSnapshot(snapshot => (
    //         setPeople(snapshot.docs.map(doc => doc.data()))
    //     ))
    // }, []);
// const [people, setPeople] = useState([]);

useEffect(() => {
  async function fetchData() {
    // cuz base url already set up in axios.js
    const request = await axios.get("http://localhost:8000/get_cards");

    // whatever the request.data comes back us
    console.log(request)
    setPeople(request.data);
  }

  fetchData();
}, []);
    return (
        <div>
           <div className="tinderCards__cardContainer">
           {people.map((person) =>(
               <TinderCard
               className="swipe"
               key={person.name}
               preventSwipe={['up', 'down']}>
                <div 
                style={{ backgroundImage: `url(${person.imgUrl})` }}
                className="card">
           <h3>{person.name}</h3>
                </div>
               </TinderCard>
           ))}
           </div>

        </div>
    )
}

// Exporting the above TinderCards function.
export default TinderCards;
