// External Dependencies
import React from "react";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";

// Internal Dependecies
import "./landingPage.css";

// Declaring a function called Landingpage which returns a div element.
function Landingpage() {
  return (
    <div class="landing_header">
      <nav>
        <ul>
          <li>
            <a href="#">learn</a>
          </li>
          <li>
            <a href="#">support</a>
          </li>
          <li>
            <a href="#">Download</a>
          </li>
          <li>
            <Link to="/login">
              <IconButton>
                <button id="login">Login</button>
              </IconButton>
            </Link>
          </li>
        </ul>
      </nav>

      <div class="text-box">
        <h1>start something epic!</h1>
        <Link to="/signup">
          <button id="createbtn" onclick="window.location.href='register.html'">
            create account
          </button>
        </Link>
      </div>
      <a
        href="https://play.google.com/store/apps/details?id=com.tinder&hl=en"
        class="download"
      >
        Download
      </a>
    </div>
  );
}

// Exporting the above Landingpage function.
export default Landingpage;
