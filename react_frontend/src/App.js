// External Dependencies
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

// Internal Dependecies
import "./App.css";
import Header from "./components/header/Header";
import SwipeButtons from "./components/swipeButtons/SwipeButtons";
import TinderCards from "./components/tinderCards/TinderCards";
import Chats from "./components/chats/Chats";
import ChatScreen from "./components/chats/ChatScreen";
import Editprofile from "./components/editProfile/edit_profile";
import SignUp from "./components/userRegistration/Signup";
import LogIn from "./components/userRegistration/Login";
import Forgot from "./components/userRegistration/Forgot";
import Landingpage from "./components/landingpage/landingPage";

// Declaring a function called App which returns a div element.
import Home from './components/Home/Home'
function App() {
  return (
    <div className="App">
      {/* Router enables the navigation among views of various components*/}
      <Router>
        {/* The switch component looks through all of its child routes and
         it displays the first one whose path matches the current URL. */}
        <Switch>
          <Route exact path="/" component={Landingpage} />

          <Route path="/signup" component={SignUp} />

          <Route path="/login" component={LogIn} />

          <Route path="/home" component={Home} />

          <Route path="/forgot" component={Forgot} />

          <Route path="/chats/:person">
            <Header backButton="/chats" />
            <ChatScreen />
          </Route>

          <Route path="/chats">
            <Header backButton="/home" />
            <Chats />
          </Route>

          <Route path="/edit-profile">
            <Header backButton="/home" />

            <Editprofile />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

// Exporting the above App function.
export default App;
